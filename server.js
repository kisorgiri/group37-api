const http = require('http');
const file_op = require('./task');

const server = http.createServer(function (request, response) {
    // request or 1st argument is http request object
    // response or 2nd argument is http response object
    console.log('client connected to server');
    console.log('http method >>', request.method);
    console.log('http url >>', request.url);

    // request response cycle must be completed
    // there can single response for 1 request


    if (request.url === '/write') {
        file_op.w('kishor.txt', 'hello and welcome')
            .then(function (data) {
                response.end('success in write')
            })
            .catch(function (err) {
                response.end('failure in write'+err)
            })
    }
    else if (request.url === '/read') {

    } else {
        response.end('nothing to perform')
    }

    // response send garna lai
    // response.end('welcome to nodejs server')

    // in http protocol
    // for communication we need endpoint
    // endpoint ==> combination of method and url
    // GET /Login POST/Register

    // req-res handler 
    // this callback will execute regardless of http method and url


})

server.listen('4000', '127.0.0.1', function (err, done) {
    if (err) {
        console.log('error in listening >>', err);
    }
    else {
        console.log('server listening at port 4000 ');
        console.log('press CTRL + C to exit')
    }
})


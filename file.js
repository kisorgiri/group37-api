const fs = require('fs');

// write , read,rename, remove

// write

// fs.writeFile('./files/broadwa.txt', 'welcome to nodejs', function (err, done) {
//     if (err) {
//         console.log('error is >>', err);
//     } else {
//         console.log('success in wirte >>', done)
//     }
// });

const file_op = require('./task');

// execution part
file_op.w('abc.js', 'hi i am nodejs')
    .then(function (data) {
        console.log('succes in my own write >>', data)
    })
    .catch(function (err) {
        console.log('failure in my write >>', err)
    });

// task 
// prepare a function to write
// handle result of that function


// read 

fs.readFile('./files/kishor.txt', 'UTF-8', function (err, done) {
    if (err) {
        console.log('error in read', err)
    } else {
        console.log('success in read >>', done)
    }
})

// preapre function to read
// hanlde result of function
// separate function in another file and run from another file


// rename
fs.rename('./files/broadwa.txt', './files/broadway.js', function (err, done) {
    if (err) {
        console.log('error in renaming >>', err);
    }
    else {
        console.log('rename success full >', done)
    }
})

// preapre function to read
// hanlde result of function
// separate function in another file and run from another file


// remove
fs.unlink('./files/kishor.txt', function (err, done) {
    if (err) {
        console.log('error inr emoving >>>', err);
    }
    else {
        console.log('succes sin removing >>', done)
    }
})


// preapre function to read
// hanlde result of function
// separate function in another file and run from another file

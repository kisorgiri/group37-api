const userRoute = require('./../controllers/users.controller');
const authRoute = require('./../controllers/auth.controller');
const productRoute = require('./../components/products/proudct.route');

// load middleware
const authenticate = require('./../middlewares/authentication')

const router = require('express').Router();

router.use('/auth', authRoute);
router.use('/user',authenticate, userRoute);
router.use('/product', productRoute);


module.exports = router;

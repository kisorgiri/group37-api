const express = require('express');
const PORT = 4040;
const morgan = require('morgan');
const path = require('path')
const cors = require('cors');

const app = express();
// entire express framework is in app

// event stuff
const events = require('events');
const myEv = new events.EventEmitter();

app.use(function (req, res, next) {
    req.myEv = myEv;
    next();
})
myEv.on('err', function (err, res) {
    console.log('at custom error hanlder ', err)
    res.json(err);
})

// event stuff
// socket stuff
require('./socket')(app)

require('./db_init');

const API_ROUTE = require('./routes/api.route')

// load third party middleware
app.use(morgan('dev'))
app.use(cors());
// load inbuilt middleware
// incoming request must be parsed according to content-type
// parser for x-www-form-urlencoded
app.use(express.urlencoded({
    extended: true
}))
// this middleware will parse incoming request with content type application/x-www-formurlencoded
// and then add the parsed data in request body object
// json parser
app.use(express.json());
// serve static files  
// app.use(express.static('uploads')) // serve for internal usage
app.use('/file', express.static(path.join(process.cwd(), 'uploads'))) // serve for external request
// TODO use other inbuilt middleware

// load routing level middleware
// mount 
app.use('/api', API_ROUTE)

// application level middleware as 404 handler
app.use(function (req, res, next) {
    res.status(404) // staus set in response
    next({
        msg: 'Not Found',
        status: 404 //this status code is part of response data object
    })
})


// Error handling middleware
// error handling middleware is middleware with 4 arugments
// 1st argument is for error
// 2nd 3rd and 4th are req res and next
// error handling middleware will not came into action in between req-res cycle
// error handling middleware must be called to get executed
// next with argument will invoke error handling middleware
// throught the application whereever we have scope of next we can call next with argument
app.use(function (err, req, res, next) {
    console.log("i am application level middleware", err);
    // TODO set status code in response for appropriate req-res cycle completion
    res.status(err.status || 400);
    res.json({
        msg: err.msg || err,
        status: err.status || 400
    })
})

app.listen(process.env.PORT || 8080, function (err, done) {
    if (err) {
        return console.log('error in listening  >>>', err);
    }
    console.log('server listening at port ' + process.env.PORT);
    console.log('press CTRL + C to exit');
})


// middleware

// middleware is a function that has access to 
// http request object
// http response object and 
// next middleware function reference

// middleware function came into action in between req-res cycle

// syntax

// function(req,res,next){
    // req or 1st argument placeholder is http request object.
    // res or 2nd argument placeholder is http response object
    // next or 3rd argument is for next middleware reference
// }

// configuration block of middleware

// .use,.<http method> (get,put post delete) , .all vitra use vako function is middleware
// use is widely used
// so app.use is configuration block for middlewares

// eg
// app.use(function(req,res,next){
// logic for application
// })

// the order of middleware is very very important


// types of middleware
// 1. Application level middleware
// above mentioned middleware are application level middleware
// developer aafaile req,res and next ko scope sanga kaam garna painecha

// 2. Routing level middleware
// 3. inbuilt middleware
// 4. third party middleware
// 5 error handling middleware


// incoming data must be parsed in server
//request header will always have Content-Type 
// content type will hold value about the type of content


// globals in nodejs
// process, __dirname,__filename,global
// console.log('working directory path >>', __dirname);
// console.log('root directory path root way >>', process.cwd())

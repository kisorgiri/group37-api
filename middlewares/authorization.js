// authorization is followed with authentication

module.exports = function (req, res, next) {
    if (req.user.role === 1) {
        next();
    }
    else {
        next({
            msg: "Authorization Failed! You Dont have access",
            status: 401
        })
    }
}


const config = require('./configs')
const socket = require('socket.io')
var users = [];
module.exports = function (app) {

    // initilize socket server
    const io = socket(app.listen(config.SOCKET_PORT), {
        cors: {
            allow: '*'
        }
    })
    io.on('connection', function (client) {
        console.log('client connected to socket server', client.id);
        var id = client.id;
        client.on('new-user', function (username) {
            users.push({
                id: id,
                name: username
            })
            client.emit('users', users)
            client.broadcast.emit('users', users)
        })

        client.on('new-message', function (data) {
            client.emit('reply-message-own', data) // requesting client
            // other then requesting client connected to server
            // client.broadcast.emit('reply-message', data)
            // emit to selected client
            client.broadcast.to(data.receiverId).emit('reply-message', data)
        })

        client.on('disconnect', function () {
            users.forEach((user, index) => {
                if (user.id === id) {
                    users.splice(index, 1)
                }
            })
            client.broadcast.emit('users', users)

        })
    })
}
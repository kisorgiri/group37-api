// database
// database is a container where application data are stored
// memory partition for storage

// data manipulation occurs in database
// database management system (DBMS)

// two group od DBMS
// 1. Relational DBMS
// 2. Distributed DBMS

// 1. RDBMS
// mysql, sql-lite, postgres, mssql
// a. table based design
// application entities are tables
// eg LMS ===> Books, Reviews, Users ===> table
// b.each records are called tuples/rows
// c.schema based solution
// d.relation between table exists
// e.non scalable
// we cannot add proprties other then defined in schema
// f.eg mysql, sql-lite, postgres, mssql
// g SQL database

// 2. DDBMS
// a.collection based approach
// BOOKs, Reviews,Users ===> collections
// b. each record are called document
// document is a JSON
// any valid js Object can be store in DDBMS
// c. Schema less design
// d.relation doesnot exits between collections
// e.highly scalable
// f.eg mongodb, dynamodb, couchdb,redis
// g NOSQL ==> not only sql

// MongoDB ==> cli,
// ui tool
// PL ko through


// at first mongodb programme(server application) must be in our system

// for windows
// after installtaion 
// mongodb will be located  at progamme files
// copy the path upto bin folder
// this computer >>right click.> properties.>> advance settings>> environmanet variable
// user variabl,system variable

// goal ---> update your system variable's path
// IMP .. do not remove anything from existing path
// IMP for windows user >> use powershell or git bash

// shell command
// once you get >  (arrow head) interface you are ready to execute shell command
// type mongo command to access mongo shell

// show dbs ==> list all available databases

// to create database
// use <db_name>  
// if(db_name exists ) select existing database else create new database and select
// db==> prints sleected database
// show collections ==> list all available collections of selected database
// to create collection
// db.<collection_name>.insert({valid JSON})
// if(collection exists) insert new deocument in collection else
// create new collection and add record

// CRUD (db operations)
// Create
// db.<collection_name>.insert({json})
// db.<collection_name>.insertMany({json})

// Read
// db.<collection_name>.find({<query_builder>})
// db.<collection_name>.find({<query_builder>}).pretty() // format result
// db.<collection_name>.find({<query_builder>}).count() // document result
// db.<collection_name>.find({tags:{$in:['a','b']}}) // $in operator will check for any matching element
// db.<collection_name>.find({tags:{$all:['a','b']}}) // $a;; operator will check for every elements to match
// db.<collection_name>.find({"obj1.obj2.obj3":"val"})
// db.<collection_name>.find({price:{$gte:2999}})

// projection==> decide which value to fetch
// db.<collection_name>.find({query_builder},{project object with either inclusion or exclusion})

// .limit()
// .skip()
// .sort({_id:-1}) //decending order sort

// Update
// db.<collection_Name>.update({},{},{})
// 1st object ==> query_Builder 
// 2nd object will have $set key and value as payload(object) to be updated
// 3rd options ===> optionals

// Remove
// db.<collection_name>.remove({query_builder})
// NOTE:- do note leave query_builder empty it will remove all documents



// drop collection
// db.collection_name.drop();

// drop database 
// db.dataBase();



// mongoose (ODM)
// ODM/ORM ==>
// ODM ==> Object document modelling ==> document based database
// ORM -==> Object relation mapping ====> relational database


// Advantages
// 1. Schema based solution
// a.database modelling 
// b. predefined entities and their attributes with indexes
// 2. methods ==> mongoose methods
// 3.data type==> Date 
// 4.middleware ==> 
// 5.indexing is lot more easier (required ,unique,)


// Database Backup and Restore

// two format ===> 1. BSON  format
// 2. readable format (JSON and CSV)

// commands
// mongodump 
// mongorestore
// mongoimport
// mongoexport

// bson format
// backup
// command
// mongodump
// mongodump ===> it will create backup of all available databases into default dump folder
// mongodump --db <db_name> it will create backup of selected database into default dump folder
// mongodump --db<db_name> --out <path_to_destination_folder>

// restore 
// command  ===> mongorestore
// mongorestore ===> it will look after dump folder tries to restore available databases
// mongorestore --drop ===> drops existing documents from database and restore with backup content
// mongorestore <path_to_source directory>==> restore from selected location

// JSON format  and CSV
// command ==> mongoexport for backup and mongoimport for restore

// JSON
// mongoexport --db <db_name> --collection <collection_name> --out <path_to_destination_with.json extension>
// mongoexport -d <db_name> -c <collection_name> -o <path_to_destination_with.json extension>
// mongoexport --db group37db --collection users --query='{"role":1}' --out dbBackup/jsonbackup/users1.json

// import for JSON
// mongoimport --db [new or existing db] --collection [new or existing collection] source to json file

// CSV
// backup
// mongoexport --db <db_name> --collection<collection_name> --type=csv --fields 'comma(,) separated properties to be exported' --out <path_to_destination_with.csv extension>

// restore 
// mongoimport --db <new or existing db > --colletion <new or existing collection> --type=csv <path_to_source file> --headerline





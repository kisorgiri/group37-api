
const fs = require('fs');


function myWrite(fileName, content) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('./files/' + fileName, content, function (err, done) {
            if (err) {
                reject(err);
            }
            else {
                resolve(done)
            }
        })
    })

}


module.exports = {
    w: myWrite
}

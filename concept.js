// Node ===> run time environment for server side programming with JS
// 2009 ryan dahl 

// npm ==> node pacakge manager 
// similar to npm ==> yarn is another packagement tool


// npx ---> pacakge management tool


// command
// node --version
// npm --version
// npx --version

// to initiate any js project
// npm init 
// ==> it will create a pacakge.json file to initiate a project
// package.json file ==> project overall information holding file
// introductory file


//npmjs.com ==> global repository for holding packages related to JS
// 
// npm install <pacakge_name> it will downloads packages from npmjs registry

// downloaded packages are kept in node_modules folder

// package-lock.json or yarn.lock ==> locks exact version of packages during initial installation


// globally installed pacakges
// npm install --global <package_name>


// file - file communication

// module-module communication

// es5 syntax
// export syntax
// module.exports = any [number,boolean, string,fun,array,objects]

// es6 syntax
// export keyword and export default keyword

// import syntax
// es5 syntax
// const anyName= require('path to source file');
// nodejs internal module and npmjs module(node_Modules modules) import garda path dinu pardaina


// es6 syntax
// import {} from 'source'
// import anyName from 'source'

// server side applicaiton area
// web application
// API 
// IOT
// Real time application
// network level application

// Web application ==> framework [Express]
// build API

// protocol===> set of rules defined for communication


// web architecture
// MVC architecture
// 3 layers
// Model, Views and Controller

// tier architecture

// web 3 tier architecture
// tier is layer
// data layer, presentation layer, controller


// deployment
// platform ==> Heroku [aws, digital ocean, azure]
// database cloud service provider ==> Atlas (512 mb free)

// we should have account in atlas and heroku
// heroku toolbet must be installed in your machine
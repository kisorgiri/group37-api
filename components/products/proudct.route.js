const router = require('express').Router();
const productCtrl = require('./product.controller')
const Uploader = require('./../../middlewares/uploader')('image')
const authenticate = require('./../../middlewares/authentication')

router.route('/')
    .get(authenticate, productCtrl.get)
    .post(authenticate, Uploader.array('images'), productCtrl.post);

router.route('/add_review/:productId')
    .post(authenticate, productCtrl.addReview)

router.route('/search')
    .get(productCtrl.search)
    .post(productCtrl.search)

router.route('/:id')
    .get(authenticate, productCtrl.getById)
    .put(authenticate, Uploader.array('images'), productCtrl.update)
    .delete(authenticate, productCtrl.remove);

module.exports = router;

const productQuery = require('./product.query');

function getImageName(imgURL) {
    // http://localhost:4040/file/images/name
    return imgURL.split('images/')[1]
}

function get(req, res, next) {
    const condition = {};
    // todo  prepare condition
    if (req.user.role !== 1) {
        condition.vendor = req.user._id;
    }
    productQuery
        .find(condition)
        .then(function (results) {
            res.json(results)
        })
        .catch(function (err) {
            next(err)
        })

}

function post(req, res, next) {
    const data = req.body;
    // appened necessary property in data
    // images
    // vendor
    console.log('images >>', req.files)
    if (req.files && req.files.length) {
        data.images = req.files.map(function (item) {
            return item.filename
        })
    }
    data.vendor = req.user._id;

    productQuery
        .insert(data)
        .then(function (response) {
            res.json(response)
        })
        .catch(function (err) {
            next(err);
        })
}

function getById(req, res, next) {
    const condition = { _id: req.params.id }
    productQuery
        .find(condition)
        .then(function (results) {
            res.json(results[0])
        })
        .catch(function (err) {
            next(err)
        })
}

function update(req, res, next) {
    const data = req.body;
    // todo prepare data;
    if (req.files && req.files.length) {
        data.newImages = req.files.map(function (item) {
            return item.filename;
        })
    }
    // TODO if vendor issue should be fixed from BE
    // fix vendor's object issue
    // incase admin make an edit vendor information will be replaced by admin's id
    // data.vendor = req.user._id;

    // remove existing images information or covert to array as we recive string from frontend
    delete data.images;

    const filesToRemove = data.filesToRemove.split(',').map(function (item) {
        return getImageName(item)
    })
    data.filesToRemove = filesToRemove;

    productQuery
        .update(req.params.id, data)
        .then(function (response) {
            res.json(response)
        })
        .catch(function (err) {
            next(err);
        })
}

function remove(req, res, next) {
    productQuery
        .remove(req.params.id)
        .then(function (response) {
            if (!response) {
                return next({
                    msg: 'Product Not Found',
                    status: 404
                })
            }
            res.json(response)
        })
        .catch(function (err) {
            next(err);
        })
}

function search(req, res, next) {

    const data = req.method === 'GET' ? req.query : req.body;
    console.log('data is >>', data);
    const searchCondition = {};
    if (data.category)
        searchCondition.category = data.category;
    if (data.name)
        searchCondition.name = data.name;
    if (data.color)
        searchCondition.color = data.color;
    if (data.brand)
        searchCondition.brand = data.brand;
    if (data.minPrice)
        searchCondition.price = {
            $gte: data.minPrice
        }
    if (data.maxPrice)
        searchCondition.price = {
            $lte: data.maxPrice
        }
    if (data.minPrice && data.maxPrice)
        searchCondition.price = {
            $lte: data.maxPrice,
            $gte: data.minPrice
        }
    if (data.fromDate && data.toDate) {
        const fromDate = new Date(data.fromDate).setHours(0, 0, 0, 0);
        const toDate = new Date(data.toDate).setHours(23, 59, 59, 999);
        searchCondition.createdAt = {
            $lte: new Date(toDate),
            $gte: new Date(fromDate)
        }
    }
    // tags
    if (data.tags) {
        searchCondition.tags = {
            $in: data.tags.split(',')
        }
    }

    console.log('search condition >>', searchCondition)
    productQuery
        .find(searchCondition)
        .then(function (results) {
            res.json(results)
        })
        .catch(function (err) {
            next(err)
        })
}

function addReview(req, res, next) {
    // todo add user in req.body
    const data = req.body;
    data.user = req.user._id;
    productQuery
        .addReview(req.params.productId, data)
        .then(function (response) {
            res.json(response)
        })
        .catch(function (err) {
            next(err)
        })

}

module.exports = {
    get,
    post,
    getById,
    update,
    remove,
    search,
    addReview
}

const { mapReduce } = require('./product.model');
const ProductModel = require('./product.model');
const fs = require('fs');
const path = require('path')

function map_product_req(productData, product) {
    if (productData.name)
        product.name = productData.name;
    if (productData.category)
        product.category = productData.category;
    if (productData.description)
        product.description = productData.description;
    if (productData.quantity)
        product.quantity = productData.quantity;
    if (productData.modelNo)
        product.modelNo = productData.modelNo;
    if (productData.color)
        product.color = productData.color;
    if (productData.brand)
        product.brand = productData.brand;
    if (productData.price)
        product.price = productData.price;
    if (productData.costPrice)
        product.costPrice = productData.costPrice;
    if (productData.size)
        product.size = productData.size;
    if (productData.vendor)
        product.vendor = productData.vendor;
    if (productData.images)
        product.images = productData.images;
    if (productData.sku)
        product.sku = productData.sku;
    if (productData.status)
        product.status = productData.status;
    if (productData.manuDate)
        product.manuDate = productData.manuDate;
    if (productData.expiryDate)
        product.expiryDate = productData.expiryDate;
    if (productData.purchasedDate)
        product.purchasedDate = productData.purchasedDate;
    if (productData.salesDate)
        product.salesDate = productData.salesDate;
    if (productData.isReturnEligible)
        product.isReturnEligible = true;
    if (productData.setIsReturnEligibleFalse)
        product.isReturnEligible = false;
    if (productData.warrentyStatus)
        product.warrentyStatus = true;
    if (productData.setWarrentyStatusFalse)
        product.warrentyStatus = false;
    if (productData.warrentyPeriod)
        product.warrentyPeriod = productData.warrentyPeriod;
    if (productData.origin)
        product.origin = productData.origin;
    if (productData.tags)
        product.tags = typeof (productData.tags) === 'string' ? productData.tags.split(',') : productData.tags;
    if (productData.offers)
        product.offers = typeof (productData.offers) === 'string' ? productData.offers.split(',') : productData.offers;
    if (productData.orderNumber)
        product.orderNumber = productData.orderNumber;
    if (!product.discount)
        product.discount = {};
    if (productData.discountedItem)
        product.discount.discountedItem = true;
    if (productData.setDiscountedItemFalse)
        product.discount.discountedItem = false;
    if (productData.discountType)
        product.discount.discountType = productData.discountType;
    if (productData.discountValue)
        product.discount.discountValue = productData.discountValue;
}

function map_review_data(reviewData, review) {
    if (reviewData.user)
        review.user = reviewData.user
    if (reviewData.reviewPoint)
        review.point = reviewData.reviewPoint
    if (reviewData.reviewMessage)
        review.message = reviewData.reviewMessage;
}

// CRUD operation

function find(condition) {
    return ProductModel
        .find(condition)
        .sort({
            _id: -1
        })
        .populate('vendor', {
            username: 1,
            email: 1
        })
        .populate('reviews.user', {
            email: 1
        })
        .exec()
}

function insert(data) {
    const newProudct = new ProductModel({});
    map_product_req(data, newProudct)
    return newProudct.save()
}


function update(id, data) {
    return new Promise(function (resolve, reject) {
        ProductModel.findById(id, function (err, product) {
            if (err) {
                return reject(err);
            }
            if (!product) {
                return reject({
                    msg: 'Product Not Found',
                    status: 404
                })
            }
            // if product found update
            map_product_req(data, product)
            if (data.newImages) {
                product.images = product.images.concat(data.newImages)
            }
            // remove if there are any value in fileToRemove
            if (data.filesToRemove && data.filesToRemove.length) {
                // 1st remove from product images array
                // 2nd remove from server
                product.images.forEach(function (item, index) {
                    if (data.filesToRemove.includes(item)) {
                        product.images.splice(index, 1)
                        removeFileFromServer(item);
                    }
                })
            }

            product.save(function (err, updated) {
                if (err) {
                    return reject(err);
                }
                resolve(updated)
            })
        })
    })
}
function remove(id) {
    // remove
    return ProductModel.findByIdAndRemove(id)
}

function addReview(productId, reviewData) {
    return new Promise(function (resolve, reject) {
        ProductModel.findById(productId, function (err, product) {
            if (err) {
                return reject(err)
            }
            if (!product) {
                return reject({
                    msg: 'Product Not Found',
                    status: 404
                })
            }
            // map review Data
            const newReview = {};
            map_review_data(reviewData, newReview)

            product.reviews.push(newReview);
            product.save(function (err, done) {
                if (err) {
                    return reject(err);
                }
                resolve(done);
            })
        })
    })
}

// cleanup
function removeFileFromServer(filename) {
    fs.unlink(path.join(process.cwd(), 'uploads/images/' + filename), function (err, removed) {
        if (!err) {
            console.log('file removed from server')
        }
    })
}

module.exports = {
    find,
    insert,
    update,
    remove,
    addReview
}

// object shorthand

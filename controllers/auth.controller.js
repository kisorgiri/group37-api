const express = require('express');
const router = express.Router();
const UserModel = require('./../models/user.model');
const MAP_USER_REQ = require('./../helpers/map_user_req')
const Uploader = require('./../middlewares/uploader')('image')
// router is routing level middleware
const jwt = require('jsonwebtoken');
const configs = require('../configs');
const passwordHash = require('password-hash')
const randomStr = require('randomstring');
const sender = require('./../configs/email.config')

router.get('/', function (req, res, next) {
    require('fs').readFile(',dsfjlksdfsdj', function (err, done) {
        if (err) {
            return req.myEv.emit('err', err, res)
        }
    })
})


function prepareEmail(data) {
    return {
        from: 'Group 37 Store<noreply@example.com>', // sender address
        to: "prayush900@gmail.com," + data.email, // list of receivers
        subject: "Forgot Password ✔", // Subject line
        text: "Hello world?", // plain text body
        html: `
        <p>Hi <strong>${data.name}</strong>,</p>
        <p>we noticed that you are having trouble logging into our system please click the link below to reset your password</p>
        <p><a href="${data.link}" target="_blank">click here to reset password</a></p>
        <p>Regards</p>
        <p>Group37 Support Team</p>
        <p>2021</p>
        `, // html body
    }
}

function generateToken(data) {
    return jwt.sign({
        _id: data._id,
        username: data.username,
        role: data.role
    }, configs.JWT_SECRET)
}

router.post('/login', function (req, res, next) {
    const data = req.body;
    UserModel.findOne({
        $or: [
            { username: req.body.username },
            { email: req.body.username }
        ]
    })
        .then(function (user) {
            // validate verfication
            // if if user is active
            // token generation
            // res end
            if (!user) {
                return next({
                    msg: "Invalid Username",
                    status: 400
                })
            }
            // user found now verify password
            const isMatched = passwordHash.verify(req.body.password, user.password);
            if (!isMatched) {
                return next({
                    msg: 'Invalid Password',
                    status: 400
                })
            }
            // every this is fine with username and password
            if (user.status === 'inactive') {
                return next({
                    msg: "Your account is disabled please contact system administrator for support",
                    status: 400
                })
            }
            // token generation
            let token = generateToken(user)
            res.json({
                user: user,
                token: token
            })
        })
        .catch(function (err) {
            next(err);
        })

})

router.post('/register', Uploader.single('image'), function (req, res, next) {
    //req.body will have data from client
    console.log('req.body>>>', req.body);
    console.log('req.file >>', req.file);
    const request_data = req.body;
    if (req.fileTypeError) {
        return next({
            msg: 'Invalid file format',
            status: 406
        })
    }
    if (req.file) {
        request_data.image = req.file.filename;
    }
    // database operation
    const newUser = new UserModel({});
    // newUser mongoose object
    // _id,__v,default value, plugin(timestamps)
    const newMappedUser = MAP_USER_REQ(request_data, newUser)
    newMappedUser.password = passwordHash.generate(req.body.password);
    newMappedUser.save(function (err, done) {
        if (err) {
            return next(err);
        }
        res.status(200)
        res.json(done)
    })

})

router.post('/forgot-password', function (req, res, next) {
    UserModel.findOne({
        email: req.body.email
    })
        .then((user) => {
            if (!user) {
                next({
                    msg: "Email not registered yet!",
                    status: 400
                })
            }
            var randomToken = randomStr.generate(23);
            var tokenExpiry = Date.now() + 1000 * 60 * 60 * 2;
            // email sending
            var emailData = {
                name: user.username,
                email: user.email,
                link: `${req.headers.origin}/reset_password/${randomToken}`
            }

            var emailBody = prepareEmail(emailData);

            user.passwordResetToken = randomToken;
            user.passwordResetTokenExpiry = tokenExpiry;

            user.save(function (err, saved) {
                if (err) {
                    return next(err);
                }
                sender.sendMail(emailBody, function (err, done) {
                    if (err) {
                        return next(err)
                    }
                    res.json(done)
                })
            })
        })
        .catch(err => {
            next(err);
        })
})

router.post('/reset-password/:token', function (req, res, next) {
    let token = req.params.token;
    UserModel.findOne({
        passwordResetToken: token,
        passwordResetTokenExpiry: {
            $gte: Date.now()
        }
    })
        .then(user => {
            if (!user) {
                return next({
                    msg: 'Invalid or Expired Password Reset Token',
                    status: 400
                })
            }
            // if within valid time
            // if (Date.now() > new Date(user.passwordResetTokenExpiry).getTime()) {
            //     return next({
            //         msg: 'Password Reset Token Expired',
            //         status: 400
            //     })
            // }
            user.password = passwordHash.generate(req.body.password);
            user.passwordResetTokenExpiry = null;
            user.passwordResetToken = null;
            user.save(function (err, saved) {
                if (err) {
                    return next(err)
                }
                res.json(saved)
            })
        })
        .catch(err => {
            next(err);
        })
})

module.exports = router;


// sub routes are handled here


// connect to database server
// select database
// perforom db operation
// close db connection (optional)

const router = require('express').Router();
const UserModel = require('./../models/user.model')
const MAP_USER_REQ = require('./../helpers/map_user_req');
const Uploader = require('./../middlewares/uploader')('image');
const authorize = require('./../middlewares/authorization')


router.route('/')
    .get(function (req, res, next) {
        // find al user
        UserModel
            .find({})
            .sort({
                _id: -1
            })
            // .limit(1)
            // .skip(1)
            .exec(function (err, users) {
                if (err) {
                    return next(err);
                }
                res.json(users)
            })
    })
    .post(function (req, res, next) {
        // user add 
    });



router.route('/search')
    .get(function (req, res, next) {
        res.send("from user search")
    })
    .post(function (req, res, next) {

    })
    .put(function (req, res, next) {

    })
    .delete(function (req, res, next) {

    });

router.route('/:id')
    .get(function (req, res, next) {
        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "User Not Found",
                    status: 404
                })
            }
            res.json(user)
        })
    })
    .put(Uploader.single('image'), function (req, res, next) {
        if (req.fileTypeErr) {
            return next({
                msg: "Invalid File Format",
                status: 406
            })
        }
        UserModel.findOne({
            _id: req.params.id
        }, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "User not found",
                    status: 404
                })
            }
            const oldImage = user.image;
            // user found now update
            // user is also mongoose object
            const request_data = req.body;
            if (req.file) {
                request_data.image = req.file.filename;
            }
            var mappedUpdatedUser = MAP_USER_REQ(request_data, user)

            mappedUpdatedUser.save(function (err, updated) {
                if (err) {
                    return next(err);
                }
                // TODO ==> remove existing file
                // if you try to remove user.image it will remove newly added file
                res.json(updated)

                if (req.file) {

                    require('fs').unlink(process.cwd(), 'uploads/images/' + oldImage, function (err, done) {
                        if (!err) {
                            console.log('file removed')
                        }
                    })
                }
            })
        })

    })
    .delete(authorize, function (req, res, next) {
        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: 'User not found',
                    status: 404
                })
            }
            user.remove(function (err, removed) {
                if (err) {
                    return next(err);
                }
                res.json(removed);
            })
        })

    });


module.exports = router;


// static handler should be kept at top
